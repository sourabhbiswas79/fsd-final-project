package com.cts.assignment.controller;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cts.assignment.dto.ParentTask;
import com.cts.assignment.dto.Project;
import com.cts.assignment.dto.Task;
import com.cts.assignment.dto.User;
import com.cts.assignment.service.ProjectManagerService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProjectDetailsController {
	@Autowired
	ProjectManagerService projectManagerService;
	Logger logger = 	LoggerFactory.getLogger(ProjectDetailsController.class);
	
	@RequestMapping(value = "/getProjectDetails", method = RequestMethod.GET)
	@ResponseBody
    public List<Project> getProjectDetails() {
		
		return projectManagerService.getProjectDetails();
        
	
    }
	@RequestMapping(value = "/getProject", method = RequestMethod.GET)
	@ResponseBody
    public Project getProject(@RequestParam(value="projectId") int projectId) {
		logger.info("project id is "+ projectId);
		return projectManagerService.getProject(projectId);
        
	
    }
	
	@RequestMapping(value = "/addProject", method = RequestMethod.POST)
    public void addProject(@RequestBody String project) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(project);
		ObjectMapper mapper = new ObjectMapper();
		//mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    
	    Project p = new Project();
	   
logger.info(project.toString());

		try {
			p = mapper.readValue(json.toString(), Project.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.addProject(p);
        
	
    }
	
	

	@RequestMapping(value = "/updateProject", method = RequestMethod.PUT)
    public void updateProject(@RequestBody String project) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(project);
		ObjectMapper mapper = new ObjectMapper();
		//mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    
	    Project p = new Project();
	   
	    logger.info(project.toString());

		try {
			p = mapper.readValue(json.toString(), Project.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.updateProject(p);
        
	
    }
	
	
	
	@RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public void addTask(@RequestBody String task) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(task);
		ObjectMapper mapper = new ObjectMapper();
		//mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    
	    Task t = new Task();
	   
logger.info(task.toString());

		try {
			t = mapper.readValue(json.toString(), Task.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.addTask(t);
        
	
    }
	
	@RequestMapping(value = "/updateTask", method = RequestMethod.PUT)
    public void updateTask(@RequestBody String task) throws ParseException {
		logger.info("Update Task");
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(task);
		ObjectMapper mapper = new ObjectMapper();
	    Task t = new Task();
	   
	    logger.info(task.toString());

		try {
			t = mapper.readValue(json.toString(), Task.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.updateTask(t);
        
	
    }
	
	@RequestMapping(value = "/addParentTask", method = RequestMethod.POST)
    public void addParentTask(@RequestBody String parentTask) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(parentTask);
		ObjectMapper mapper = new ObjectMapper();
		//mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    
		ParentTask pt = new ParentTask();
	   
logger.info(parentTask.toString());

		try {
			pt = mapper.readValue(json.toString(), ParentTask.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.addParentTask(pt);
        
	
    }
	
	
	@RequestMapping(value = "/getParentTasks", method = RequestMethod.GET)
	@ResponseBody
    public List<ParentTask> getParentTasks(@RequestParam(value="parentTaskName" ,defaultValue="") String parentTaskName) {
		logger.info("Parent Task name is "+ parentTaskName);
		return projectManagerService.getParentTasks();
        
	
    }
	
	@RequestMapping(value = "/getParentName", method = RequestMethod.GET)
	@ResponseBody
    public String getParentName(@RequestParam(value="parentId") int parentId) {
		return projectManagerService.getParentName(parentId);
        
	
    }
	
	@RequestMapping(value = "/getParentTask", method = RequestMethod.GET)
	@ResponseBody
    public ParentTask getParentTask(@RequestParam(value="parentId") int parentId) {
		return projectManagerService.getParentTask(parentId);
        
	
    }
	@RequestMapping(value = "/getTasks", method = RequestMethod.GET)
	@ResponseBody
    public List<Task> getTasks(@RequestParam(value="projectId") int projectId) {
		logger.info("project id is "+ projectId);
		return projectManagerService.getTasks(projectId);
        
	
    }
	
	@RequestMapping(value = "/getTask", method = RequestMethod.GET)
	@ResponseBody
    public Task getTask(@RequestParam(value="taskId") int taskId) {
		return projectManagerService.getTask(taskId);
        
	
    }

	@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET)
	@ResponseBody
    public List<User> getUserDetails() {
		
		return projectManagerService.getUserDetails();
        
	
    }
	@RequestMapping(value = "/getUser", method = RequestMethod.GET)
	@ResponseBody
    public User getUser(@RequestParam(value="userId") int userId) {
		logger.info("User id is "+ userId);
		return projectManagerService.getUser(userId);
        
	
    }
	@RequestMapping(value = "/getUserForProject", method = RequestMethod.GET)
	@ResponseBody
    public User getUserForProject(@RequestParam(value="projectId") int projectId) {
		logger.info("Project id is "+ projectId);
		return projectManagerService.getUserForProject(projectId);     
	
    }
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public void addUser(@RequestBody String user) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(user);
		ObjectMapper mapper = new ObjectMapper();
	    User u = new User();
	   
        logger.info(user.toString());

		try {
			u = mapper.readValue(json.toString(), User.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.addUser(u);
        
	
    }
	
	

	@RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    public void updateUser(@RequestBody String user) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(user);
		ObjectMapper mapper = new ObjectMapper();	    
	    User u = new User();
	   
		try {
			u = mapper.readValue(json.toString(), User.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		projectManagerService.updateUser(u);
        
	
    }
	

	@RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE)
    public void deleteUser(@RequestParam(value="userId") int userId) throws ParseException {	
		
		projectManagerService.deleteUser(userId);
        
	
    }
	
	
	
}
