package com.cts.assignment.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cts.assignment.dto.User;

public interface UserManagementRepository extends MongoRepository<User,String>{

	public User findByfirstName(String firstName);

	public User findByUserId(int userId);
	
	public User findByProjectId(int projectId);

	public void deleteByUserId(int userId);
}
