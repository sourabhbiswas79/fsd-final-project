package com.cts.assignment.dao;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cts.assignment.dto.Task;

public interface TaskManagerRepository extends MongoRepository<Task,String>{

	public Task findByTaskName(String taskName);

	public Task findByTaskId(int taskId);
	
	public List<Task> findByProjectId(int projectId);

}



