package com.cts.assignment.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cts.assignment.dto.Project;


public interface ProjectManagerRepository extends MongoRepository<Project,String>{

	public Project findByProjectName(String projectName);

	public Project findByProjectId(int projectId);
	

}
