package com.cts.assignment.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cts.assignment.dto.ParentTask;

public interface ParentTaskManagerRepository extends MongoRepository<ParentTask,String>{

	public ParentTask findByparentTaskName(String parentTaskName);

	public ParentTask findByParentId(int parentId);
	

}

