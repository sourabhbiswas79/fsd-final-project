package com.cts.assignment.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.cts.assignment.controller.ProjectDetailsController;
import com.cts.assignment.dao.ParentTaskManagerRepository;
import com.cts.assignment.dao.ProjectManagerRepository;
import com.cts.assignment.dao.TaskManagerRepository;
import com.cts.assignment.dao.UserManagementRepository;
import com.cts.assignment.dto.ParentTask;
import com.cts.assignment.dto.Project;
import com.cts.assignment.dto.ProjectTaskMapping;
import com.cts.assignment.dto.Task;
import com.cts.assignment.dto.User;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;


import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

@Service
public class ProjectManagerService {

	@Autowired
	ProjectManagerRepository projectManagerRepository;

	@Autowired
	TaskManagerRepository taskManagerRepository;

	@Autowired
	ParentTaskManagerRepository  parentTaskManagerRepository;

	@Autowired
	UserManagementRepository userManagementRepository;

	@Autowired
	MongoOperations mongoOperation;
	
	@Autowired
	MongoTemplate mongoTemplate;

	Logger logger = 	LoggerFactory.getLogger(ProjectManagerService.class);
	 
	public List<Project> getProjectDetails() {

		//MongoCollection<org.bson.Document> collection = database.getCollection("task");
		 List<Project> pi = new ArrayList<>();
		//		pi.setProjectName("FinalAssignment");
		//		pi.setPriority(1);
		//		pi.setProjectId(1);
		//		pi.setStartDate(new Date());

		pi = projectManagerRepository.findAll();
	
		Aggregation agg = newAggregation(
				
				group("projectId").count().as("totalTask"),
				project("totalTask").and("projectId").previousOperation(),
				sort(Sort.Direction.DESC, "projectId")
					
			);

			//Convert the aggregation result into a List
			AggregationResults<ProjectTaskMapping> groupResults 
				= mongoTemplate.aggregate(agg, Task.class, ProjectTaskMapping.class);
			List<ProjectTaskMapping> result = groupResults.getMappedResults();
			
		result.stream().forEach(a -> logger.info(a.getTotalTask() +"******"+ a.getProjectId()));
		
		for(ProjectTaskMapping r : result)
		{
			pi.stream().forEach(p -> {
				if(p.getProjectId() == r.getProjectId()) {
					p.setTotalTask(r.getTotalTask());
				}
			});
		}

		return pi;

	}

	public void addProject(Project project) {
		List<Project> pl = projectManagerRepository.findAll();
		int id = pl.size()+1;
		project.setProjectId(id);
		projectManagerRepository.save(project);
		logger.info("Record saved."); 
	}

	public Project getProject(int projectId) {

		Project p = new Project();
		p = projectManagerRepository.findByProjectId(projectId);
		logger.info("project name::"+p.getProjectName());
		return p;
	}

	public void updateProject(Project p) {
		logger.info("project id ::"+p.getProjectId());
		//mongoOperation.update(p.getClass());
		projectManagerRepository.save(p);
	}

	public void addTask(Task t) {

		List<Task> task = taskManagerRepository.findAll();
		int id = task.size()+1;
		t.setTaskId(id);

		taskManagerRepository.save(t);
		logger.info("Record saved."); 


	}

	public void addParentTask(ParentTask pt) {


		// TODO Auto-generated method stub
		List<ParentTask> ptask = parentTaskManagerRepository.findAll();
		int id = ptask.size()+1;
		pt.setParentId(id);
		parentTaskManagerRepository.save(pt);
		logger.info("Record saved."); 


	}

	public ParentTask getParentTask(int parentId) {

		ParentTask pt = new ParentTask();

		pt = parentTaskManagerRepository.findByParentId(parentId);
		return pt;
	}

	public List<ParentTask> getParentTasks() {

		List<ParentTask> pt = new ArrayList<>();

		pt = parentTaskManagerRepository.findAll();
		return pt;
	}

	public List<User> getUserDetails() {

		List<User> u = new ArrayList<>();
		u = userManagementRepository.findAll();
		return u;

	}

	public void addUser(User user) {
		List<User> usr = userManagementRepository.findAll();
		int id = usr.size()+1;
		user.setUserId(id);
		userManagementRepository.save(user);
		logger.info("User saved."); 
	}

	public User getUser(int userId) {

		User u = new User();
		u = userManagementRepository.findByUserId(userId);
		return u;
	}

	public void updateUser(User u) {
		logger.info("User ID ::"+ u.getUserId());
		userManagementRepository.save(u);
	}

	public List<Task> getTasks(int projectId) {
		return taskManagerRepository.findByProjectId(projectId);

	}

	public Task getTask(int taskId) {
		return taskManagerRepository.findByTaskId(taskId);

	}

	public String getParentName(int parentId) {
		// TODO Auto-generated method stub
		ParentTask pt = new ParentTask();
		String name="";
		pt = parentTaskManagerRepository.findByParentId(parentId);
		if (null != pt) {
		 name = pt.getParentTaskName();
		}
		return name;
	}

	public User getUserForProject(int projectId) {
		User u = new User();
		u = userManagementRepository.findByProjectId(projectId);
		return u;
	}

	public void deleteUser(int userId) {
		userManagementRepository.deleteByUserId(userId);
	}

	public void updateTask(Task t) {
		taskManagerRepository.save(t);
	}

}
