package com.cts.assignment.dto;

import java.util.List;

public class ProjectInfo {

	private List<Project> projectDetails;

	public List<Project> getProjectDetails() {
		return projectDetails;
	}

	public void setProjectDetails(List<Project> projectDetails) {
		this.projectDetails = projectDetails;
	}
	
}
