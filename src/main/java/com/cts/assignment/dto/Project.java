package com.cts.assignment.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Project {

    @Id
	private int projectId;
	private String projectName;
	private Date startDate;
	private Date endDate;
	private int priority;
	private String completeStatus;
	private int totalTask;
	private String manager;
	
	public Project() {}
	
	public Project(String projectName, int projectId, Date startDate, Date endDate, int priority,String completeStatus, int totalTask ,String manager) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priority = priority;
		this.completeStatus = completeStatus;
		this.totalTask = totalTask;
		this.manager = manager;
	}

	

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	

	public String getCompleteStatus() {
		return completeStatus;
	}

	public void setCompleteStatus(String completeStatus) {
		this.completeStatus = completeStatus;
	}

	
	public int getTotalTask() {
		return totalTask;
	}

	public void setTotalTask(int totalTask) {
		this.totalTask = totalTask;
	}

	
	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", projectName=" + projectName + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", priority=" + priority + ", completeStatus=" + completeStatus
				+ ", totalTask=" + totalTask + ", manager=" + manager + "]";
	}
	
	

	

}
