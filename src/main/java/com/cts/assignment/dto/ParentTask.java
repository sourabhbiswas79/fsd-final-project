package com.cts.assignment.dto;

import org.springframework.data.annotation.Id;

public class ParentTask {

	@Id
	private int parentId;
	private String parentTaskName;
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getParentTaskName() {
		return parentTaskName;
	}
	public void setParentTaskName(String parentTaskName) {
		this.parentTaskName = parentTaskName;
	}

	
}
