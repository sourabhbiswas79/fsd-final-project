package com.cts.assignment.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Task {

@Id
private int taskId ;
private int projectId ;
private int parentId ;
private String taskName ;
private Date startDate ;
private Date  endDate;
private int priority;
private String status;


public Task() {
	super();
	// TODO Auto-generated constructor stub
}


public Task(int taskId, int projectId, int parentId, String taskName, Date startDate, Date endDate, int priority,String status) {
	super();
	this.taskId = taskId;
	this.projectId = projectId;
	this.parentId = parentId;
	this.taskName = taskName;
	this.startDate = startDate;
	this.endDate = endDate;
	this.priority = priority;
	this.status = status;
}


public int getTaskId() {
	return taskId;
}
public void setTaskId(int taskId) {
	this.taskId = taskId;
}
public int getProjectId() {
	return projectId;
}
public void setProjectId(int projectId) {
	this.projectId = projectId;
}
public int getParentId() {
	return parentId;
}
public void setParentId(int parentId) {
	this.parentId = parentId;
}
public String getTaskName() {
	return taskName;
}
public void setTaskName(String taskName) {
	this.taskName = taskName;
}
public Date getStartDate() {
	return startDate;
}
public void setStartDate(Date startDate) {
	this.startDate = startDate;
}
public Date getEndDate() {
	return endDate;
}
public void setEndDate(Date endDate) {
	this.endDate = endDate;
}
public int getPriority() {
	return priority;
}
public void setPriority(int priority) {
	this.priority = priority;
}


public String getStatus() {
	return status;
}


public void setStatus(String status) {
	this.status = status;
}


@Override
public String toString() {
	return "Task [taskId=" + taskId + ", projectId=" + projectId + ", parentId=" + parentId + ", taskName=" + taskName
			+ ", startDate=" + startDate + ", endDate=" + endDate + ", priority=" + priority + ", status=" + status
			+ "]";
}
	
}
