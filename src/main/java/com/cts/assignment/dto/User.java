package com.cts.assignment.dto;

import org.springframework.data.annotation.Id;

public class User {

	@Id
	private int userId;
	private String firstName;
	private String lastName;
	private long empId;
	private int taskId;
	private int projectId;
	
	
	public User() {
		super();
		
	}


	public User(int userId, String firstName, String lastName, long empId, int taskId, int projectId) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.empId = empId;
		this.taskId = taskId;
		this.projectId = projectId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public long getEmpId() {
		return empId;
	}


	public void setEmpId(long empId) {
		this.empId = empId;
	}


	public int getTaskId() {
		return taskId;
	}


	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}


	public int getProjectId() {
		return projectId;
	}


	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}


	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", empId=" + empId
				+ ", taskId=" + taskId + ", projectId=" + projectId + "]";
	}
	
	
	
}
