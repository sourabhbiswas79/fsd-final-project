package com.cts.assignment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.cts.assignment.dao.ParentTaskManagerRepository;
import com.cts.assignment.dao.ProjectManagerRepository;
import com.cts.assignment.dto.ParentTask;
import com.cts.assignment.dto.Project;
import com.cts.assignment.dto.Task;
import com.cts.assignment.dto.User;

import ch.qos.logback.core.net.SyslogOutputStream;

class ProjectManagerServiceTest {
	
	@Mock
	ProjectManagerService projectManagerService;
	
	@Mock
	ProjectManagerRepository projectManagerRepository;
	
	@Mock
	ParentTaskManagerRepository  parentTaskManagerRepository;

	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetProjectDetails() {
		
		List<Project> projectList = projectManagerService.getProjectDetails();
		assertNotNull(projectList);
	}

	@Test
	void testGetProject() {
		Project p = new Project();
		p.setProjectId(1);
		p.setProjectName("test");
		when(projectManagerRepository.findByProjectId(1)).thenReturn(p);
		Project pp = projectManagerRepository.findByProjectId(1);
		assertNotNull(p);
		assertEquals(1, pp.getProjectId());
	}

	@Test
	void testGetParentTask() {
		ParentTask ppt = new ParentTask();
		ppt.setParentId(1);
		ppt.setParentTaskName("test task");
		when(parentTaskManagerRepository.findByParentId(4)).thenReturn(ppt);
		
		assertNotNull(ppt);
		assertEquals("test task", ppt.getParentTaskName());
	}

	@Test
	void testGetParentTasks() {
		List<ParentTask> p = projectManagerService.getParentTasks();
		assertNotNull(p);
	}

	@Test
	void testGetUserDetails() {
		List<User> u = projectManagerService.getUserDetails();
		assertNotNull(u);
	}

	@Test
	void testGetTasks() {
		List<Task> t = projectManagerService.getTasks(1);
	
		assertNotNull(t);
	}


}
